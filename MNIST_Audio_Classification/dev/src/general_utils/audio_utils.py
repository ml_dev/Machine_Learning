import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import warnings
warnings.filterwarnings("ignore")

import wave
import librosa
#import pyaudio
import numpy as np
import os.path as osp
import sounddevice as sd

from playsound import playsound
from scipy.io.wavfile import write


NORM_FACTOR = 32768

def norm_audio(samples):
    return (samples / NORM_FACTOR).astype(np.float32)  
              
def denorm_audio(samples):
        return (samples * NORM_FACTOR).astype(np.int16)
         
def decode_audio(samples):
    try:
        return np.fromstring(samples, dtype=np.int16)
    except:
        return np.empty(0, dtype=np.int16)      

def resample_audio(input_file, output_file, inrate=None, outrate=None):
        samples, sr = librosa.load(input_file, sr=inrate)
        samples = librosa.resample(samples, orig_sr=sr, target_sr=outrate)
        samples = denorm_audio(samples) 
        write_audio(output_file, samples, outrate)      

def setup_audio_recorder(sample_rate=16000, channels=1):  
    return pyaudio.PyAudio().open(format=pyaudio.paInt16, 
                                  channels=channels, 
                                  rate=sample_rate, 
                                  input=True)

def record_audio(recorder_obj=None, chunk = 256, decode=True, norm=False): 
    samples = recorder_obj.read(chunk)
    if decode:
        samples = decode_audio(samples)    
    if norm:
        samples = norm_audio(samples)    
    return samples                                  

def read_audio(filename, sample_rate=None, norm=False):
    """
    Read wave file as mono.
    Args:
        - filename (str) : wave file / path.
        - sample_rate (int) : samples per second
        - norm (bool) : flag determine the range of samples
    Returns:
        tuple of audio data and sampling rate.
    """
    samples, sr = librosa.load(filename, sr=sample_rate, mono=True)

    if not norm:
        samples = denorm_audio(samples)
    
    return samples, sr

def write_audio(output_file_path, sig, fs):
    """
    Read wave file as mono.
    Args:
        - output_file_path (str) : path to save resulting wave file to.
        - sig            (array) : signal/audio array.
        - fs               (int) : sampling rate.

    Returns:
        tuple of sampling rate and audio data.
    """
    write(filename=output_file_path, rate=fs, data=sig)

def get_wave_read_obj(wave_file):
    if 'wav' not in osp.splitext(osp.basename(wave_file))[1]:
        raise RuntimeError('File extension not supported')
    else:
        wf = wave.open(wave_file, 'rb')
        return wf

def get_wave_write_obj(wave_file='sample.wav', sample_rate=16000, channels=1):
    if 'wav' not in osp.splitext(osp.basename(wave_file))[1]:
        raise RuntimeError('File extension not supported')
    else:
        wf = wave.open(wave_file, 'wb')
        wf.setnchannels(channels)
        wf.setsampwidth(pyaudio.PyAudio().get_sample_size(pyaudio.paInt16))
        wf.setframerate(sample_rate)
        return wf

def read_frames(wave_obj, frame_size, norm=False, decode=True):
    samples = wave_obj.readframes(frame_size)
    if decode:
        samples = decode_audio(samples)     
    if norm:
        samples = norm_audio(samples)
    return samples

def play_audio(filename):
    playsound(filename)

def play_samples(samples, sample_rate):
    sd.play(samples, sample_rate)
    sd.wait()
         
       
if __name__ == '__main__':
    os.system('clear')
    print('Audio!!! \n') 
    sample_rate = 16000
    expected_nsample = int(3.2 * sample_rate)
    current_sample = 0
    while True:
        stream = setup_audio_recorder()
        data = record_audio(recorder_obj=stream, chunk = 4000)
        current_sample += len(data)
        print(data.shape)
        if current_sample >= expected_nsample:
            break 
        
