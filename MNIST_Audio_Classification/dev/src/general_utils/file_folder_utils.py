import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import warnings
warnings.filterwarnings("ignore")

from pathlib import Path
from general_utils.general_utils import sort_string_list

def get_filenames(folder_path=None, ext='wav', sorting=False):
    folder = Path(folder_path)
    ext = '*.{}'.format(ext)
    file_list = list(folder.rglob(ext))
    file_list = [str(fn) for fn in file_list]
    if sorting:
        sort_string_list(file_list)
    return file_list
    
def get_audio_filenames(folder_path=None, sorting=False):
    file_format = ['*.wav', '*.ogg', '*.m4a', '*.mp3', '*.aac']
    folder = Path(folder_path)
    audiofile_list = []
    for frmt in file_format:
        file_list = list(folder.rglob(frmt))
        file_list = list(map(str, file_list))
        audiofile_list.extend(file_list)
    if sorting:
        sort_string_list(audiofile_list)
    return audiofile_list
