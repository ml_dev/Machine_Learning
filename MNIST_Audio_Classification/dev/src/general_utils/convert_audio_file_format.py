import os
import warnings

os.system('clear')
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
warnings.filterwarnings("ignore")

import argparse

from pathlib import Path
from os import path as osp
from pydub import AudioSegment
from general_utils.audio_utils import read_audio
from general_utils.audio_utils import get_wave_write_obj

class Convert_File_Format:

    def apply(self, process_name, input_file, output_file):
        getattr(self, process_name)(input_file, output_file)
   
    def mp4_to_mp3(self, input_file, output_file=None):
        sound = AudioSegment.from_file(input_file)
        sound.export(output_file, format="mp3")
        
    def m4a_to_wav(self, input_file, output_file=None):
        sound = AudioSegment.from_file(input_file,  format= 'm4a')
        sound.__data
        sound.export(output_file, format='wav')

    def aac_to_wav(self, input_file, output_file=None):
        sound = AudioSegment.from_file(input_file,  format= 'aac')
        sound.export(output_file, format='wav')  
      
    def mp3_to_ogg(self, input_file, output_file=None):
        sound = AudioSegment.from_mp3(input_file)
        sound.export(output_file, format="ogg")
        
    def mp3_to_wav(self, input_file, output_file=None):
        sound = AudioSegment.from_mp3(input_file)
        sound.export(output_file, format="wav")        
    
    def ogg_to_mp3(self, input_file, output_file=None):
        sound = AudioSegment.from_ogg(input_file)
        sound.export(output_file, format="mp3")
    
    def ogg_to_wav(self, input_file, output_file=None):
        sound = AudioSegment.from_ogg(input_file)
        sound.export(output_file, format="wav")
    
    def wav_to_mp3(self, input_file, output_file=None):
        sound = AudioSegment.from_wav(input_file)
        sound.export(output_file, format="mp3")
        
    def wav_to_ogg(self, input_file, output_file=None):
        sound = AudioSegment.from_wav(input_file)
        sound.export(output_file, format="ogg")
           
    def sterio_to_mono(self, input_file, output_file=None):
        sound = AudioSegment.from_mp3(input_file)
        sound = sound.set_channels(1)
        sound.export(output_file, format="wav") 

    def raw_to_wav(self, input_file, output_file=None, sr = 16000):
        with open(input_file, "rb") as f:
            data = f.read()
            wf = get_wave_write_obj(wave_file=output_file, sample_rate=sr, channels=1)
            wf.writeframesraw(data)  

    def wav_to_raw(self, input_file, output_file=None):
        samples, sr = read_audio(input_file) 
        with open(output_file, 'wb') as f:
            f.write(samples)
    

if __name__ == '__main__':
    os.system('clear')
    print('File Format Casting!!! \n')
    convert_ff = Convert_File_Format()
    fu = File_Utils()
    lu = List_Utils()

    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, 
                        default='',
                        help='data_dir', 
                        required=False)
    parser.add_argument('--sample_rate', type=int, 
                        default=48000,
                        help='samples per second', 
                        required=False) 
    parser.add_argument('--in_format', type=str, 
                        default='wav',
                        help='in_format', 
                        required=False) 
    parser.add_argument('--out_format', type=str, 
                        default='raw',
                        help='out_format', 
                        required=False) 
    args = parser.parse_args()
    
    data_dir = args.data_dir
    sample_rate = args.sample_rate
    in_format = args.in_format
    out_format = args.out_format

    process_name = '{}_to_{}'.format(in_format, out_format)

    if data_dir[-1] == '/': data_dir = data_dir[:-1]
    
    dst_dir = '{}_{}'.format(data_dir, out_format)
    Path(dst_dir).mkdir(parents=True, exist_ok=True)
    if process_name == 'sterio_to_mono':
        in_format = out_format = 'wav'

    file_list = fu.get_filenames(folder_path=data_dir, ext=in_format)
    file_list = lu.sort(file_list)
    remaining_files = len(file_list)
    for fn in file_list:
        basename = osp.basename(fn)
        print('{:06} - {}'.format(remaining_files, basename))
        basename_only = osp.splitext(basename)[0]
        dst_fname = '{}/{}.{}'.format(dst_dir, basename_only, out_format)
        convert_ff.apply(process_name, fn, dst_fname) 
        remaining_files -= 1 
 

    
