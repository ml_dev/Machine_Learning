import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import warnings
warnings.filterwarnings("ignore")

import re
import uuid
import random
import hashlib
from pathlib import Path

def sort_string_list(alist):
    def atoi(text):
        return int(text) if text.isdigit() else text
    def natural_keys(text):
        return [atoi(c) for c in re.split(r'(\d+)', text)]
    alist.sort(key=natural_keys)

def get_unique_name():
    _id = uuid.uuid4() 
    unique_id = '_'.join([str(_id.fields[0]), str(_id.fields[-1])])
    return unique_id 
    
def shuffle_list(alist):
    for i in range(10):
        random.shuffle(alist)
    return alist 
    
def gethash(files):
    BLOCK_SIZE = 65536
    file_hash = hashlib.sha256()    
    with open(files, 'rb') as f:
        fb = f.read(BLOCK_SIZE)
        while len(fb) > 0: # While there is still data being read from the file
            file_hash.update(fb) # Update the hash
            fb = f.read(BLOCK_SIZE)
    return file_hash.hexdigest()             
