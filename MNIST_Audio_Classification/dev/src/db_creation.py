import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import warnings
warnings.filterwarnings("ignore")

import shutil
import random
import numpy as np
from math import ceil, floor
from os import path as osp
from general_utils.audio_utils import read_audio, write_audio
from general_utils.file_folder_utils import get_filenames

def trim_silence(audio, noise_threshold=100):
    """ Removes the silence at the beginning and end of the passed audio data

    :param audio: numpy array of audio
    :param noise_threshold: the maximum amount of noise that is considered silence
    :return: a trimmed numpy array
    """
    start = None
    end = None

    for idx, point in enumerate(audio):
        if abs(point) > noise_threshold:
            start = idx
            break

    # Reverse the array for trimming the end
    for idx, point in enumerate(audio[::-1]):
        if abs(point) > noise_threshold:
            end = len(audio) - idx
            break

    return audio[start:end]
    
def dataset_preparation(data_dir):
    filenanmes = []
    speaker_names = []
    sample_rates = []
    nsamples_list = []
    file_list = get_filenames(folder_path=data_dir, ext='wav', sorting=False)
    remaining_files = len(file_list)
    for fname in file_list:
        basename = osp.basename(fname)
        print('{:05} -- {}'.format(remaining_files, basename))
        samples, sr = read_audio(fname)
        filenanmes.append(basename)
        speaker_names.append(basename.split('_')[1])
        sample_rates.append(sr)
        nsamples_list.append(len(samples))
        remaining_files -= 1
        
    result = all(element == sample_rates[0] for element in sample_rates)
    if result:
       sample_rate = sample_rates[0]
    else:
        print('Error')   
       
    speaker_list = list(set(speaker_names))  
    
    return file_list, sample_rate, speaker_list
    
def remove_silence(file_list, sample_rate):
    remaining_files = len(file_list)
    dst_dir = 'DB/remove_silence'
    os.makedirs(dst_dir, exist_ok=True)
    clip_duration_list = []
    processed_filelist = []
    for fname in file_list:
        basename = osp.basename(fname)
        print('{:05} -- {}'.format(remaining_files, basename))
        samples, sr = read_audio(fname) 
        samples = trim_silence(samples, noise_threshold=150)
        clip_duration_list.append(len(samples)) 
        dst_fname = '{}/{}'.format(dst_dir, basename)
        write_audio(dst_fname, samples, sample_rate)
        processed_filelist.append(dst_fname)
        remaining_files -= 1 
     
    return processed_filelist, clip_duration_list   
    
def equalize_duration(file_list, clip_duration_list, sample_rate):
    remaining_files = len(file_list)
    dst_dir = 'DB/equalize_duration'
    os.makedirs(dst_dir, exist_ok=True)
    processed_filelist = []
    max_clipdur = round(max(clip_duration_list)/sample_rate)
    max_nsample = int(max_clipdur*sample_rate)

    for fname in file_list:
        basename = osp.basename(fname)
        samples, sr = read_audio(fname) 

        if len(samples)<max_nsample:
            required_samples = max_nsample - len(samples)
            left_add = int(required_samples/2)
            right_add = required_samples-left_add
            samples1 = ([0]*left_add + list(samples) + [0]*right_add)
        
        elif len(samples)>max_nsample:
            remove_samples = len(samples)-max_nsample
            left_remove = int(remove_samples/2)
            right_remove = remove_samples-left_remove
            samples1 = samples[left_remove:(len(samples)-right_remove)]

        samples1 = np.array(samples1).astype(np.int16)
        dst_fname = '{}/{}'.format(dst_dir, basename)
        write_audio(dst_fname, samples1, sample_rate)
        processed_filelist.append(dst_fname)
        remaining_files -= 1 
     
    return processed_filelist   
    
def train_val_test(file_list, sample_rate, speaker_list):
    n_speakers = len(speaker_list)
    test_n_speakers = ceil(n_speakers*0.1)   
    val_n_speakers = ceil(n_speakers*0.2) 
    
   
    test_speakers = random.sample(speaker_list, test_n_speakers) 
    for i in test_speakers:
        speaker_list.remove(i) 

    val_speakers = random.sample(speaker_list, val_n_speakers) 
    for i in val_speakers:
        speaker_list.remove(i) 
    train_speakers = speaker_list  
    remaining_files = len(file_list)
    for fname in file_list:
        basename = osp.basename(fname)
        print('{:05} -- {}'.format(remaining_files, basename))
        
        speaker_name = basename.split('_')[1]
        label = basename.split('_')[0]
        if speaker_name in test_speakers:
            dst_dir = 'DB/test/{}'.format(label)
            os.makedirs(dst_dir, exist_ok=True)
            dst_fname = '{}/{}'.format(dst_dir, basename)
            shutil.copy(fname, dst_dir)
            
        if speaker_name in val_speakers:
            dst_dir = 'DB/val/{}'.format(label)
            os.makedirs(dst_dir, exist_ok=True)
            dst_fname = '{}/{}'.format(dst_dir, basename)
            shutil.copy(fname, dst_dir)  
            
            
        if speaker_name in train_speakers:
            dst_dir = 'DB/train/{}'.format(label)
            os.makedirs(dst_dir, exist_ok=True)
            dst_fname = '{}/{}'.format(dst_dir, basename)
            shutil.copy(fname, dst_dir)      
    
        remaining_files -= 1
          
       
if __name__ == '__main__':
    os.system('clear')
    data_dir = 'DB/recordings'
    file_list, sample_rate, speaker_list = dataset_preparation(data_dir)
    processed_filelist, clip_duration_list = remove_silence(file_list, sample_rate)
    processed_filelist = equalize_duration(processed_filelist, clip_duration_list, sample_rate)
    train_val_test(processed_filelist, sample_rate, speaker_list)
