import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import warnings
warnings.filterwarnings("ignore")

import shutil
import random
import numpy as np
from math import ceil, floor
from os import path as osp
from python_speech_features import mfcc
from general_utils.audio_utils import read_audio, write_audio
from general_utils.file_folder_utils import get_filenames
from sklearn.utils.class_weight import compute_class_weight
from tensorflow.keras import losses
from tensorflow.keras import utils
from tensorflow.keras import callbacks
from tensorflow.keras.models import Model
from tensorflow.keras import regularizers
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import SGD, Adadelta,Adam, Nadam, RMSprop
from tensorflow.keras.layers import Input, InputLayer, Dense, Conv1D, Flatten, Dropout, MaxPool1D, MaxPooling1D, BatchNormalization, Activation, Bidirectional, GRU, Concatenate, LSTM, LayerNormalization, GlobalAveragePooling1D


    
def create_model(input_shape=(199, 13), n_class=10):
    datainp = Input(shape = input_shape)
    x=datainp

    x = Conv1D(filters=32, kernel_size=3, strides=1, activation='relu', padding='same')(x)
    x = LayerNormalization(axis=2)(x)
    x = MaxPool1D(strides=2)(x)
    x = Conv1D(filters=32, kernel_size=3, strides=1, activation='relu', padding='same')(x)
    x = MaxPool1D(strides=2)(x)
    x = Conv1D(filters=64, kernel_size=3, strides=1, activation='relu', padding='same')(x)
    x = MaxPool1D(strides=2)(x)
    x = Conv1D(filters=64, kernel_size=3, strides=1, activation='relu', padding='same')(x)
    x = MaxPool1D(strides=2)(x)
    x = Conv1D(filters=64, kernel_size=3, strides=1, activation='relu', padding='same')(x)
    x = GlobalAveragePooling1D()(x)
    x = Dropout(0.1)(x)
    x = Dense(n_class, activation='softmax')(x)
            
    model = Model(inputs=datainp, outputs=x)
    
    return model
    
                                                
def read_train_val_data(train_fetures, train_labels, val_featres, val_labels):
    train_x = np.load(train_fetures)
    train_y = np.load(train_labels)
    val_x = np.load(val_featres)
    val_y = np.load(val_labels)              
    return train_x, train_y, val_x, val_y  

def GetClassWeights(yTrain):
        yTrain = np.concatenate(yTrain) 
        class_weights = compute_class_weight('balanced', np.unique(yTrain), yTrain)
        d_class_weights = dict(enumerate(class_weights))
        return d_class_weights

def train(model, train_x, train_y, val_x, val_y):
     model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
     #model.compile(optimizer=Adam(), loss=losses.SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'],)
     
     
     model_saver = callbacks.ModelCheckpoint('model.h5', 
                                                  monitor           = 'accuracy',
                                                  verbose           = 1, 
                                                  save_best_only    =True, 
                                                  save_weights_only =False, 
                                                  mode              ='auto', 
                                                  period         =1)
     model.fit(train_x, train_y, 
               validation_data =(val_x, val_y), 
               epochs          = 20, 
               callbacks       =[model_saver],
               shuffle         = True,
               batch_size      = 8)
                
if __name__ == '__main__':
    os.system('clear')
    
    train_fetures = 'npy_files/train_X.npy'
    train_labels = 'npy_files/train_Y.npy'
    
    val_featres = 'npy_files/val_X.npy'
    val_labels = 'npy_files/val_Y.npy'
    
    train_x, train_y, val_x, val_y  = read_train_val_data(train_fetures, train_labels, val_featres, val_labels)
    
    input_shape = (train_x.shape[1], train_x.shape[2])
    n_class = len(np.unique(train_y))
    
    train_y = utils.to_categorical(train_y, num_classes=n_class)
    val_y = utils.to_categorical(val_y, num_classes=n_class)
    
    model = create_model(input_shape=input_shape, n_class=n_class)

    train(model, train_x, train_y, val_x, val_y)
    
