import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import warnings
warnings.filterwarnings("ignore")

import shutil
import random
import numpy as np
from math import ceil, floor
from os import path as osp
from python_speech_features import mfcc
from general_utils.audio_utils import read_audio, write_audio
from general_utils.file_folder_utils import get_filenames

def extract_features(data_dir, key = 'train'):
    features_list = []
    labels_list = []

    label_list = sorted(os.listdir(data_dir))
    
    file_list = get_filenames(folder_path=data_dir, ext='wav', sorting=False)
    remaining_files = len(file_list)
    for fname in file_list: 
        basename = osp.basename(fname)
        label = fname.split('/')[-2]
        label_id = label_list.index(label)

        print('{:05} -- {}'.format(remaining_files, basename))
        samples, sr = read_audio(fname)
        
        feat_vec = mfcc(samples, samplerate=sr, winlen=0.025, winstep=0.01, numcep=13, nfilt=26, nfft=512,)
        features_list.append(feat_vec)
        labels_list.append(label_id)
        remaining_files -= 1
    feature_array = np.array(features_list)  
    labels_array = np.array(labels_list)  
    
    dst_dir = 'npy_files'  
    os.makedirs(dst_dir, exist_ok=True)    
    
    feature_fname = '{}/{}_X.npy'.format(dst_dir, key)
    label_fname = '{}/{}_Y.npy'.format(dst_dir, key)
    np.save(feature_fname, feature_array, allow_pickle = False)
    np.save(label_fname, labels_array, allow_pickle = False)
    
    
if __name__ == '__main__':
    os.system('clear')
    extract_features('DB/train', key = 'train')
    extract_features('DB/val', key = 'val')
    extract_features('DB/test', key = 'test')
